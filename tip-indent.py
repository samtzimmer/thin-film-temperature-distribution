#! /usr/bin/env python2

"""
Copyright (c) (2018) Samuel Zimmermann <samuel.zimmermann@protonmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import fenics as fn
import fenicshelper as fh

# ---------- parameters ----------

mesh_name = 'tip-indent'    # path to the mesh file witout extension
gmsh_cmd = '-3' # gmsh command

# mesh parameters
mesh_params = {'angle': 15, 
               'height': 5e-8, 
               'width': 1e-6, 
               'thickness': 350e-9, 
               'hdmsh': 2e-9, 
               'ldmsh': 1e-7,
               'depth': 2e-6,
               'rtip': 1e-8,
               }

# lookup region to number in mesh
regions = {
        'heater': 1,
        'mirror': 2,
        'heat sink': 3,
        'air': 4,
        'body': 5
        }

# simulation parameters
u0 = 298                    # heat sink temperature and initial condition (K)
uh = 676                    # heater temperature (K)
T = 6e-6                    # simulation time
n_timesteps = 2            # number of time steps
dt = T / n_timesteps        # time step size
diff_thermal = 1.6e-7       # thermal diffusivity of silk fibroin(m2/s)

# Create Files
vtkfile = fn.File('tip-indent.pvd')

# ---------- script ----------

# Create and load mesh
fh.create_mesh(mesh_name, mesh_params, gmsh_cmd, update=True)
mesh, pr, fr = fh.load_mesh(mesh_name)

# Function space
V = fn.FunctionSpace(mesh, 'P', 1)

# Integration elements
dx = fn.Measure('dx', subdomain_data = pr)

# Material constants
alpha = fn.Constant(diff_thermal) # Thermal diffusivity

# Boundary conditions
bcs = [
       fn.DirichletBC(V, uh, fr, regions['heater']),
       fn.DirichletBC(V, u0, fr, regions['heat sink'])
       ]

# Initial conditions
u_n = fn.interpolate(fn.Constant(u0), V)

# Variational problem
u = fn.TrialFunction(V)
v = fn.TestFunction(V)

# left and right 
a = v*u*dx + alpha*dt*fn.dot(fn.grad(v),fn.grad(u))*dx
L = v*u_n*dx

# Time-stepping
u = fn.Function(V)
t = 0

# loop time steps
for n in range(n_timesteps):
    
    # Update current time
    t += dt
    
    # Compute solution
    fn.solve(a == L, u, bcs)
    
    # Store solution
    vtkfile << (u, t)
    
    # Update previous solution
    u_n.assign(u)
    
    print('{:d}/{:d}'.format(n, n_timesteps))